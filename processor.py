# encoding: utf-8
#!/usr/bin/python

from operator import attrgetter
import datetime
import os
from pdb import set_trace
import random
import shutil
import sys
import time
import urllib
import json
import twitter

this_dir = os.path.split(__file__)[0]
config = json.load(open(os.path.join(this_dir, "config.json")))
in_dir = config.get('in_dir')
out_dir = config.get('out_dir')
title = config['title']

def first_words(text, max_length=50):
    if isinstance(text, str):
        text = text.decode("utf8")
    text = text.strip()
    if not ' ' in text:
        return ''
    words = text[:text.rindex(' ', 0, max_length)] + '...' #u'…'
    return words.strip()

class Postcard:

    KEYS = ["inscription", "comment", "tags", "postcard-front", "postcard-back"]
    KEY_NAMES = {'inscription' : 'Inscription',
                 'comment' : "Leonard's comments",
                 "postcard-front" : "Postcard front",
                 "postcard-back" : "Postcard back"}

    SMALL = [150, 100]
    MEDIUM = 1200

    def __init__(self, dir, name):
        self.dir = dir
        self.name = name
        self.text_file = os.path.join(self.dir, name + ".txt")
        self.parse()
        self.previous = None
        self.next = None

    def __str__(self):
        return "%s: date %s, tags %s" % (self.name, self.date, ",".join(self.tags))

    def sanityCheck(self):
        if not os.path.exists(self.text_file):
            print "%s: text file missing." % self.name
        if not os.path.exists(self.file('a')):
            print "%s: front missing." % self.name
        if not os.path.exists(self.file('b')):
            print "%s: back missing." % self.name
        if len(self.tags) > 5:
            print "%s: More than 5 tags: %s" % (self.name, self.tags)

    def parse(self):
        keys = {}
        current_key = None
        current_value = ""
        for line in open(self.text_file):
            if len(line) == 1:
                continue
            line = line.rstrip()
            if line[0] == ' ':
                # Continuation line
                if current_key is None:
                    raise ValueError(
                        "First line starts with blank in %s" % self.text_file)
                current_value += line
            else:
                # First line of new key
                if ':' not in line:
                    raise ValueError(
                        "Missing blank at BOL in %s?" % self.text_file)
                if current_key is not None:
                    keys[current_key] = current_value
                current_key, current_value = line.split(':', 1)
                if current_key not in self.KEYS:
                    raise ValueError("Invalid vocabulary item in %s: %s" %
                                     (self.text_file, current_key))
            if current_key is not None:
                keys[current_key] = current_value
            self.keys = keys
        if 'tags' in self.keys:
            self.tags = self.keys['tags'].strip().split(" ")
        else:
            self.tags = []

    @property
    def subdirectory(self):
        year = self.year
        if year == "0000" or int(year) < 1981:
            return "Unknown and pre-1981"
        elif int(year) >= 1991:
            return "1991-1992"
        else:
            return year

    @property
    def year(self):
        return self.name[:4]

    @property
    def date(self):
        d = self.name[:8]
        try:
            d = time.strptime(d, "%Y%m%d")
            return time.strftime("%Y/%m/%d", d)
        except ValueError:
            if self.year != "0000":
                return self.year
            else:
                return "Unknown date"

    def file(self, side):
        return os.path.join(self.dir, self.name + side + ".jpg")

    def image_file(self, out_dir, side, size):
        return os.path.join(out_dir, self.image_path(side, size))

    def image_path(self, side, size):
        return os.path.join(self.subdirectory, size, self.name + side + ".jpg")

    @property
    def front_thumb_path(self):
        return self.image_path('a', 'thumbs')

    @property
    def back_thumb_path(self):
        return self.image_path('b', 'thumbs')

    def gallery_file(self, out_dir):
        return os.path.join(out_dir, self.gallery_path).replace(".html", ".bhtml")

    @property
    def gallery_path(self):
        return os.path.join(self.subdirectory, self.name + ".html")

    @property
    def url(self):
        pass

    def as_tweet(self, collection):
        url = collection.root_url + self.gallery_path
        images = [self.image_file(collection.out_dir, side, "images") for side in ['a', 'b']]
        images = [x for x in images if os.path.exists(x)]
        # Max length for a tweet with three links + a space
        max_length = 91
        body = ''
        for key in ["inscription", "postcard-front", "postcard-back",
                    "comment"]:
            if key in self.keys:
                body = url + " " + " ".join("#" + tag.replace("-","") for tag in self.tags)
        return self, body, images

    def images(self, out_dir, force):
        from PIL import Image
        for side in 'a', 'b':
            filename = self.file(side)
            if not os.path.exists(filename):
                continue
            # Make a small thumbnail: 150 by 100
            thumbnail_file = self.image_file(out_dir, side, 'thumbs')

            dir = os.path.split(thumbnail_file)[0]
            if not os.path.exists(dir):
                os.makedirs(dir)

            if not os.path.exists(thumbnail_file) or force == "force":
                image = Image.open(filename)
                size = image.size
                if size[0] < size[1]:
                    small_size = [self.SMALL[1], self.SMALL[0]]
                else:
                    small_size = self.SMALL
                image.thumbnail(small_size)
                print "Saving %s" % thumbnail_file
                image.save(thumbnail_file)

            # Make a medium-sized image file: 1000 by X
            image_file = self.image_file(out_dir, side, 'images')
            dir = os.path.split(image_file)[0]
            if not os.path.exists(dir):
                os.makedirs(dir)
            if not os.path.exists(image_file) or force == "force":
                image = Image.open(filename)
                size = image.size
                if size[0] < size[1]:
                    medium_size = [int(size[0] / (float(size[1])/ self.MEDIUM)),
                                   self.MEDIUM]
                else:
                    medium_size = [self.MEDIUM,
                                   int(size[1] / (float(size[0])/ self.MEDIUM))]
                image.thumbnail(medium_size)
                print "Saving %s" % image_file
                image.save(image_file)

    def output_thumb(self, out, side="a", in_dir=True,
                     alt=None, before='', after=''):
        if side == 'a':
            thumb_path = self.front_thumb_path
        else:
            thumb_path = self.back_thumb_path

        gallery_path = self.gallery_path
        if in_dir:
            gallery_path = "../" + gallery_path
            thumb_path = "../" + thumb_path

        if alt is None:
            alt_text = ""
        else:
            alt_text = 'alt="%s" title="%s"' % (alt, alt)

        out.write('<a href="%s">%s' % (gallery_path, before))
        out.write('<img border="0" src="%s" %s />%s</a>\n'
                  % (thumb_path, alt_text, after))

    def output_summary(self, out, in_dir=True):
        self.output_thumb(out, "a", in_dir)
        text = None
        for key in ['inscription', 'comment', 'postcard-back',
                    'postcard-front']:
            if key in self.keys:
                text = self.keys[key]
                break
        if text is not None:
            out.write('<p class="summary">%s</p>\n' % first_words(text))

    def asHTML(self):
        a = '<p><img width="80%%" src="%s/%s"></p>' % (SITE_ROOT, self.image_path("a", "images"))
        a += '<p><img width="80%%" src="%s/%s"></p>' % (SITE_ROOT, self.image_path("b", "images"))
        for key in ["inscription", "postcard-front", "postcard-back",
                    "comment"]:
            if key in self.keys:
                a += '<p><b>%s:</b></p> <blockquote>%s</blockquote>' % (self.KEY_NAMES[key],
                                                                        self.keys[key])
        a += '<p><b>See also:</b> '
        for tag in self.tags:
            a += '<a href="%s/tags/%s.html">%s</a> ' % (SITE_ROOT, tag, tag)
        a += '</p>'
        return a

    def gallery(self, out_dir, arg):
        gallery_file = self.gallery_file(out_dir)
        dir = os.path.split(gallery_file)[0]
        if not os.path.exists(dir):
            os.makedirs(dir)
        if not os.path.exists(gallery_file) or arg == 'force':
            out = open(gallery_file, "w")
            out.write("TITLE %s: Postcard from %s\n" % (SITE_TITLE, self.date))
            out.write("RSS %s/rss.xml\n" % SITE_ROOT)
            out.write("STYLE %s/postcards.css\n" % SITE_ROOT)
            out.write("\n")
            out.write('<p><b><a href="%s">%s</a>: %s</b></p>' % (SITE_ROOT, SITE_TITLE, self.date))
            out.write(self.asHTML())
            out.write('<hr />')
            out.write('<div class="nav">')
            if self.previous is not None:
                self.previous.output_thumb(out, alt="Previous", before="&lt;-")

            out.write('| <a href="./">%s</a> | <a href="../">Index</a> |'
                      % self.subdirectory)
            if self.next is not None:
                self.next.output_thumb(out, alt="Next", after="-&gt;")
            out.write('</div>')
            out.close()

class Collection:

    def open_file(self, path):
        if not path.startswith('/'):
            path = os.path.join(this_dir, path)
        if not os.path.exists(path):
            print "%s does not exist." % path
            return None
        return open(path)

    def load_filenames(self, key):
        if not key in self.config:
            return []
        path = self.config[key]
        return [x.strip() for x in self.open_file(path)]

    def __init__(self, config_path):
        self.confg = self.open_file(config_path)
        self.in_dir = config['in_dir']
        self.out_dir = config['out_dir']
        self.root_url = config['root_url']
        self.chosen_file = config.get('chosen_file')
        self.config = config
        self.refresh()

    def refresh(self):
        self.chosen = self.load_filenames('chosen_file')

        self.available = []
        self.cards = []
        self.cards_by_year = {}
        self.cards_by_tag = {}
        self.cards_by_name = {}

        for filename in os.listdir(self.in_dir):
            if filename.endswith(".txt"):
                card = Postcard(self.in_dir, filename[:-4])
                self.cards.append(card)
                if card.name not in self.chosen:
                    self.available.append(card)

        previous = None
        for card in self.sortedCards():
            subdirectory = card.subdirectory
            self.cards_by_year.setdefault(subdirectory, []).append(card)
            for tag in card.tags:
                self.cards_by_tag.setdefault(tag, []).append(card)
            self.cards_by_name[card.name] = card
            if previous is None:
                previous = card
            else:
                card.previous = previous
                previous.next = card
                previous = card

    def sortedCards(self):
        def cardkey(card):
            if card.date == "Unknown date":
                return 0
            return card.name

        for card in sorted(self.cards, key=cardkey):
            yield card

    def _outputGallery(self, out, title, cards, in_dir=True):

        out.write("TITLE %s: %s\n" % (SITE_TITLE, title))
        out.write("STYLE %s/postcards.css\n" % SITE_ROOT)
        out.write("\n")
        out.write("<h1>%s: %s</h1>" % (SITE_TITLE, title))
        self._outputGalleryTable(out, cards, in_dir)
        if in_dir:
            link = "../"
        else:
            link = "./"
        out.write('<hr />')
        out.write('<div class="nav">| <a href="%s">Index</a> |</p>' % link)

    def _outputGalleryTable(self, out, cards, in_dir=True):
        out.write("<table>\n")
        out.write("<tr>\n")
        i = 0
        for card in cards:
            out.write("<td>\n")
            card.output_summary(out, in_dir=in_dir)
            out.write("</td>\n")
            i += 1
            if i % 5 == 0:
                out.write("</tr>\n")
        out.write("</table>\n")

    def yearGallery(self, year):
        year_dir = os.path.join(self.out_dir, year)
        index = os.path.join(year_dir, "index.bhtml")
        out = open(index, "w")
        self._outputGallery(out, year, self.cards_by_year[year])
        out.close()

    def tagGallery(self, tag):
        file = os.path.join(self.out_dir, 'tags', tag + '.bhtml')
        dir = os.path.split(file)[0]
        if not os.path.exists(dir):
            os.makedirs(dir)
        out = open(file, "w")
        self._outputGallery(out, "'%s'" % tag,
                            self.cards_by_tag[tag])
        out.close()

    def guide(self):
        file = os.path.join(self.out_dir, 'guide.html')
        out = open(file, "w")

        def yearkey(year):
            if year.startswith("Unknown"):
                return "0000"
            else:
                return year

        out.write("<p>Recent additions:</p>\n")
        stack = RecentCardStack(self, RECENT_STACK_PATH, RSS_PATH)
        cards = [self.cards_by_name[name]
                 for name, date in stack.stack]
        self._outputGalleryTable(out, cards, False)

        out.write("<p>Browse by year:</p>\n")
        out.write("<ul>\n")
        for year in sorted(self.cards_by_year.keys(), key=yearkey):
            out.write('<li><a href="%s/">%s</a> (%d)</li>\n' % (urllib.quote(year), year, len(self.cards_by_year[year])))
        out.write("</ul>\n")

        out.write("<p>Or browse by tag:</p>\n")
        out.write("<ul>\n")
        for tag in sorted(self.cards_by_tag.keys()):
            out.write('<li><a href="tags/%s.html">%s</a> (%d)</li>\n'
                      % (tag, tag, len(self.cards_by_tag[tag])))
        out.write("</ul>\n")
        out.close()

    # Implementations of commands
    def listCards(self, arg):
        for card in self.sortedCards():
            print " %s" % card

    def sanityCheck(self, arg):
        for card in self.sortedCards():
            card.sanityCheck()
        for tag, cards in self.cards_by_tag.items():
            if len(cards) < 3:
                print "Few cards tagged %s, might be typo: %s" % (
                    tag, ", ". join(c.name for c in cards))

    def images(self, arg):
        for card in self.sortedCards():
            card.images(self.out_dir, arg)

    def galleries(self, arg):
        self.images(arg)
        self.html(arg)

    def html(self, arg):
        for card in self.sortedCards():
            card.gallery(self.out_dir, arg)
        for year in self.cards_by_year:
            self.yearGallery(year)
        for tag in self.cards_by_tag:
            self.tagGallery(tag)
        self.guide()

    def choose_tweet(self):
        choice = random.choice(self.available)
        return choice.as_tweet(self)
        
    def no_longer_available(self, postcard):
        out = open(self.chosen_file, 'a')
        out.write(postcard.name)
        out.write("\n")
        
commands = { 'list' : 'listCards',
             'sanity' : 'sanityCheck',
             'images': 'images',
             'html' : 'html',
             'galleries': 'galleries',
             'tweet' : 'tweet',
         }

def printUsage():
    print "Usage: %s %s [arg]" % (sys.argv[0], "|".join(commands.keys()))
    sys.exit()

if __name__ == '__main__':
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        printUsage()
    command = sys.argv[1]
    if len(sys.argv) > 2:
        arg = sys.argv[2]
    else:
        arg = None
    if command not in commands:
        printUsage()
    collection = Collection(config)
    getattr(collection, commands[command])(arg)
