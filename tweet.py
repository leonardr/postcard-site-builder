#!/usr/bin/python
from pdb import set_trace
import json
import unicodedata
import base64

from processor import Collection

import os
import random
import sys
import shutil
import twitter
from email.mime.multipart import (
    MIMEMultipart,
)
from email.mime.image import MIMEImage
from email.generator import Generator

# Use Sycorax credentials
TWITTER_CONSUMER_KEY = "oCjvd8o8tyN8gOCM1K7pA"
TWITTER_CONSUMER_SECRET = "gbVO0zKaXBarImJjLUWr0hiffAxpCl7dk2rylK6enU"

base_dir = os.path.split(__file__)[0]
token_file = os.path.join(base_dir, "tokens.txt")
TWITTER_TOKEN, TWITTER_SECRET = [x.strip() for x in open(token_file)]

config_path = os.path.join(base_dir, "config.json")
collection = Collection(config_path)

class Twitter(twitter.Twitter):
    """Simple wrapper around the python-twitter library for Olipy.
    """
    def __init__(self):
        oauth = twitter.OAuth(
            TWITTER_TOKEN, TWITTER_SECRET,
            TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET)
        super(Twitter, self).__init__(auth=oauth)

    def with_images(self, tweet, images):
        if not images:
            return api.statuses.update(status=tweet)
        image = images[0]
        #multipart = MIMEMultipart()
        #for image in images:
        #    data = open(image, "rb").read()
        #    multipart.attach(MIMEImage(data))
        data = open(image, "rb").read()
        encoded = base64.b64encode(data)
        #encoded_tweet = base64.b64encode(tweet.encode("utf8"))
        params = {"media[]": data, "status": tweet.encode("utf8")}
        self.statuses.update_with_media(**params)

api = Twitter()
postcard, tweet, images = collection.choose_tweet()
api.with_images(tweet, images)
    
collection.no_longer_available(postcard)
